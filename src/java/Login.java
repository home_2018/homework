/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Orkhan Huseynli
 */
public class Login extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String uname=request.getParameter("uname");
           String pass=request.getParameter("pass");
           String adminLogin = this.getServletConfig().getInitParameter("login");
           String adminPass = this.getServletConfig().getInitParameter("password");
           
           HttpSession session=request.getSession();
           
           String name=(String) session.getAttribute("username");
           String password=(String) session.getAttribute("password");
        try (PrintWriter out = response.getWriter()) {
           
           if(uname.equals(name) && pass.equals(password)&&
                   !name.equals("")  && !password.equals("")){
              RequestDispatcher rd = request.getRequestDispatcher("home_main.jsp");
               rd.include(request, response);
              
              session.setAttribute("authorized","true");
                
           }
           // in this part we give the authorization to the admin of our page that he/she can access to the page without
           // registering to the page
         if(!uname.equals("")&& 
                 pass.equals(adminPass)&&uname.equals(adminLogin) && !pass.equals("")) {
               RequestDispatcher rd = request.getRequestDispatcher("home_main.jsp");
               rd.include(request, response);
              
              session.setAttribute("authorized","true");
           }
           else if(!uname.equals(name)){
               RequestDispatcher rq= request.getRequestDispatcher("index.html");
                rq.include(request,response);
                 out.println("<h2+ style=\"color:black; margin-left:0%\">" + "Please make sure your username is correct! </h2>");
           }
           else{
               RequestDispatcher rq= request.getRequestDispatcher("index.html");
                rq.include(request,response);
                 out.println("<h2 style=\"color:black; margin-left:0%\">" + "Please make sure your password is correct! </h2>");
           }
          
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
