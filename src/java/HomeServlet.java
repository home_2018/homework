/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Elgun Majidov
 */
public class HomeServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession();
        response.setContentType("text/html;charset=UTF-8");
         
      
        RequestDispatcher rd = request.getRequestDispatcher("home_main.jsp");
           rd.include(request, response);
      
        try (PrintWriter out = response.getWriter()) {
                   Cookie[] cookies = request.getCookies();
          
          if(cookies != null){
            
              for(int i = 0; i < cookies.length; i++){
        
            out.println("<script>"
                    + "document.getElementById(\"header\").style.backgroundColor=\""+cookies[i].getName()+"\""
                    + "</script>");
              }
        }
              String bgcolor = (String)session.getAttribute("bgcolor");
              if (bgcolor!=null)
        {
            out.println("<script>"
                    + "document.getElementById(\"header\").style.backgroundColor=\""+bgcolor+"\""
                    + "</script>");
        }
          
            
           String logout = request.getParameter("logout");
            if (logout!=null && !logout.equals("")){
                session.invalidate();
                response.sendRedirect("index.html");
               
            }
        }
    }

   

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
