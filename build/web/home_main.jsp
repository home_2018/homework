<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>HOME</title>
    <meta charset="UTF-8">
    <meta name="page_id" content="home">
    <meta name="author" content="ohuseynli2018">
    <meta name="author" content="emajidov2018">
    <meta name="tags" content="website, homework, html, css, learning, 'ADA' University">
</head>

<style>
	body {
		font-family: Tahoma, Geneva, sans-serif;
	}
	#header {
		
		color: white;
		text-align: center;
		padding: 5px;
	}
	#menu {
		line-height: 30px;
		background-color: #eeeeee;
		height: 500px;
		width: 160px;
		float: left;
		padding: 5px;
	}
	#info {
		width: 350px;
		float: left;
		padding: 10px;
	}
	#footer {
		background-color: black;
		color: white;
		clear: both;
		text-align: center;
		padding: 5px;
	}
	a {
		float: left;
		width: 10em;
		color: white;
		text-decoration:none;
		background-color: black;
		border-bottom: 1px solid white;
	}
	a:hover {
		background-color: #999;
	}
	ul {
		list-style-type:none;
		float: left;
		padding: 0;
	}
	nav ul ul {
		display: none;
	}
	nav ul li:hover > ul {
		display: block;
	}
	nav ul il li {
		float: none;
	}


</style>
<body>
    <%
      String isAuthorised = (String)session.getAttribute("authorised");
        if (isAuthorised!=null && isAuthorised.equals("yes"))
        {
            
        }
        else
        {
            response.sendRedirect("index.html");
        }
   %>
   <div id="header"  style="background-color: black;">

    <img src="logo.png" width="100" height="100">
     <div style="text-align: right;">
      <form action="HomeServlet">
                   Change header color:    <select name="bgcolor">
                     <option value="red">Red</option>"
                     <option value="green">Green</option>"
                     <option value="blue">Blue</option>"
                     <option value="cyan">Cyan</option>"
                     <option value="black">Black</option>"
                     <option value="pink">Pink</option>"
                     <option value="yellow">Yellow</option>"
                     </select>
                     <input type="submit" style="background-color: black; color: white" >
                     </form>
    </div >
   </div >
    <!-- ... -->
    <div id="menu"  >
    <nav>
    <ul>
    	<li><a href="HomeServlet">Home</a></li>
		
        <li><a href="#">Members</a>
        	<ul>
            	<li><a href="orkhan.html">Orkhan Huseyn</a></li>
                <li><a href="elgun.html">Majidov Elgun</a></li>
            </ul>
        </li>
        <li><a href="photos.html">Photos</a></li>
        <li><a href="about.html">About</a></li>
        
    </ul>
    </nav>
    </div>
   
    <%
            String color = request.getParameter("bgcolor");
            if (color!=null && !color.equals("")){
                out.println("<h3> Background color is <span style=\"color:"+color+";background-color:white\">"+color+"</span></h3>");
                out.println("<script>"
                        + "document.getElementById(\"header\").style.backgroundColor=\""+color+"\";"
                        + "</script>");
                session.setAttribute("bgcolor", color);
            }
            
        %>
    <!-- ... -->
    <div>
    <h1>Welcome to our official web page!</h1>
    <p> Hi, dear friends,
    </p>
    <p name="text">This is our fist web page designed using HTML and CSS. It was our home task and fortunately we accomplished to do it.
    	We hope Vusal Khalilov won't begrudge his A+ from us. We are the champs! May be tha page is not good enough but we
        will be able to design much more pretty sites! The number of our teachers is increasing time by time, so we have 
        millions of teacher! We have millions of mistakes! Our mistakes are our masters!
    </p>

    <div >
        <form action="HomeServlet" method="get"  >
            <input type="submit" name="logout" value="Sign Out" style="background-color: black; color: white" id="button">
        </form>
    </div>
    </div>
    <!-- ... -->
    <div id="footer">
    Copyright ©HOME GROUP 2015
    </div>
   
</body>
</html>

